<?php

use Carnauba\DA\Pdv\Caixa;

error_reporting(E_ALL);
ini_set('display_errors', 'On');
require_once '../../bootstrap.php';


$logo = 'data://text/plain;base64,'. base64_encode(file_get_contents('../imagens/logo.jpg'));

try {
    
    class empresa
    {
        public $razaosocial;
        public $cnpj;
        public $logradouro;
        public $numero;
        public $bairro;
        public $cep;
        public $telefone;
        public $email;
        public $site;
        public $logomarca;
    }

    $emp = new empresa;
    $emp->razaosocial = 'JORGE AUGUSTO BESERRA DE SOUZA 95874127372';
    $emp->cnpj = '99.999.999/0001-00';
    $emp->rgie = 'ISENTO';
    $emp->logradouro = 'Rua Antonio Costa Mendes';
    $emp->numero = '235';
    $emp->complemento = 'CASA CD';
    $emp->bairro = 'VILA PERY';
    $emp->municipio = 'FORTALEZA';
    $emp->uf = 'CE';
    $emp->cep = '60730175';
    $emp->telefone = '85 32927961';
    $emp->email = 'sabidos@sabidos.com.br';
    $emp->site = 'www.sabidos.com.br';
    $emp->logomarca = 'IMAGEN';

    class pessoa
    {
        public $razaosocial;
        public $cnpj;
        public $logradouro;
        public $numero;
        public $complemento;
        public $bairro;
        public $cep;
        public $telefone;
        public $email;
        public $site;
        public $logomarca;
    }

    $pes = new pessoa;
    $pes->razaosocial = 'JORGE AUGUSTO BESERRA DE SOUZA';
    $pes->cpfcnpj = '958.741.273-72';
    $pes->rgie = '99010263941';
    $pes->logradouro = 'Rua Antonio Costa Mendes';
    $pes->numero = '235';
    $pes->complemento = 'Casa C';
    $pes->bairro = 'VILA PERY';
    $pes->municipio = 'FORTALEZA';
    $pes->uf = 'CE';
    $pes->cep = '60730175';
    $pes->telefone = '85 32927961';
    $pes->email = 'jorgebeserra@gmail.com';

    class item
    {
        public $item;
        public $quantidade;
        public $codigo;
        public $descricao;
        public $localizacao;
        public $vlrunitario;
        public $vlracrescimos;
        public $vlrdescontos;
        public $vlrtotal;
        public $situacao;
    }



    $itens = array();

    for ($i = 1; $i <= 10; $i++) {
        $item = new item;
        $item->item = $i;
        $item->quantidade = 1;
        $item->codigo = 'B17025056';
        $item->descricao = 'PAPEL MAXPLOT- 170MX250MX56GRS 3 PAPEL MAXPLOT- 170MX250MX56GRS 3 PAPEL MAXPLOT- 170MX250MX56GRS 3';
        $item->localizacao = 'A11';
        $item->vlrunitario = 10;
        $item->vlracrescimos = 0;
        $item->vlrdescontos = 0;
        $item->vlrtotal = 10;
        $item->situacao = 'N'; // N - Normal - C - Cancelado

        array_push($itens, $item);
    }



    class req
    {
        public $requisicao;
        public $situacao;
        public $emissao_dt;
        public $cancelamento_dt;
        public $conclusao_dt;
        public $xtipomovimento;
        public $itens;
        public $vendedor;
        public $formasrecebimento;
    }

    $obj = new req;
    $obj->empresa = $emp;
    $obj->requisicao = 1;
    $obj->situacao = 'ABERTA';
    $obj->emissao_dt = '2019-01-01';
    $obj->cancelamento_dt = '2019-01-01';
    $obj->conclusao_dt = '2019-01-01';
    $obj->xtipomovimento = 'VENDA BALCÃO';
    $obj->pessoa = $pes;
    $obj->itens = $itens;
    $obj->vendedor = 'JORGE AUGUSTO';
    $obj->formasrecebimento = 'Dinheiro';

    //echo "<pre>";
    //print_r($obj);
    //echo "</pre>";

    $danfe = new Caixa( $obj,'P', 'A4', $logo, 'I', '');
    $id = $danfe->montaPDF();
    $pdf = $danfe->render();
    //o pdf porde ser exibido como view no browser
    //salvo em arquivo
    //ou setado para download forçado no browser 
    //ou ainda gravado na base de dados
    header('Content-Type: application/pdf');
    echo $pdf;
} catch (InvalidArgumentException $e) {
    echo "Ocorreu um erro durante o processamento :" . $e->getMessage();
}    

