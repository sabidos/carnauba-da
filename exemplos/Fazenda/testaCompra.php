<?php

use Carnauba\DA\Fazenda\Compra;
use Carnauba\DA\Fazenda\imprimirCompra;

error_reporting(E_ALL);
ini_set('display_errors', 'On');
require_once '../../bootstrap.php';


$logo = 'data://text/plain;base64,'. base64_encode(file_get_contents('../imagens/logo.jpg'));

try {
    
    class empresa
    {
        public $razaosocial;
        public $cnpj;
        public $logradouro;
        public $numero;
        public $bairro;
        public $cep;
        public $telefone;
        public $email;
        public $site;
        public $logomarca;
    }

    $emp = new empresa;
    $emp->nomerazao = 'JORGE AUGUSTO BESERRA DE SOUZA';
    $emp->cpf_cgc = '99.999.999/0001-00';
    $emp->rgie = 'ISENTO';
    $emp->logradouro = 'Rua Antonio Costa Mendes';
    $emp->numero = '235';
    $emp->complemento = 'CASA CD';
    $emp->bairro = 'VILA PERY';
    $emp->municipio = ['descricao'=> 'FORTALEZA'];
    $emp->uf = ['descricao' => 'CE'];
    $emp->cep = '60730175';
    $emp->telefone = ['ddd' => '85', 'numero' => '32927961'];
    $emp->email = 'sabidos@sabidos.com.br';
    $emp->site = 'www.sabidos.com.br';
    $emp->logomarca = 'IMAGEN';

    class fornecedor
    {
        public $razaosocial;
        public $cnpj;
        public $logradouro;
        public $numero;
        public $bairro;
        public $cep;
        public $telefone;
        public $email;
        public $site;
        public $logomarca;
    }

    $forn = new fornecedor;
    $forn->nomerazao = 'JORGE AUGUSTO BESERRA DE SOUZA 95874127372';
    $forn->cpf_cgc = '99.999.999/0001-00';
    $forn->rgie = 'ISENTO';
    $forn->logradouro = 'Rua Antonio Costa Mendes';
    $forn->numero = '235';
    $forn->complemento = 'CASA CD';
    $forn->bairro = 'VILA PERY';
    $forn->municipio = 'FORTALEZA';
    $forn->uf = 'CE';
    $forn->cep = '60730175';
    $forn->telefone = '85 32927961';
    $forn->email = 'sabidos@sabidos.com.br';
    $forn->site = 'www.sabidos.com.br';
    $forn->logomarca = 'IMAGEN';



    class animal
    {
        public $ficha;
        public $brinco1;
        public $brinco2;
        public $tatuagem;
        public $peso;
        public $raca;
        public $sexo;
        public $categoria;
        public $link;
    }

    class situacao
    {
        public $id;
        public $descricao;
    }

    class categoria
    {
        public $id;
        public $descricao;
    }


    class raca
    {
        public $id;
        public $descricao;
    }


    class animal_pai
    {
        public $brinco1;
        public $brinco2;
        public $tatuagem;
        public $peso;
        public $raca;
        public $sexo;
        public $categoria;
    }

    class animal_mae
    {
        public $brinco1;
        public $brinco2;
        public $tatuagem;
        public $peso;
        public $raca;
        public $sexo;
        public $categoria;
    }

    class fazenda
    {
        public $id;
        public $sigla;
        public $descricao;
    }

    $dadosFazenda = new fazenda();
    $dadosFazenda->id = 1;
    $dadosFazenda->descricao = 'Menino';
    $dadosFazenda->sigla = 'FMJ1';


    $situacao = new situacao;
    $situacao->id = 1;
    $situacao->descricao = 'FAZENDA';

    $categoria = new categoria;
    $categoria->id = 1;
    $categoria->descricao = 'NOVILHA';

    $raca = new raca;
    $raca->id = 1;
    $raca->descricao = 'NELORE';


    $an_pai = new animal_pai;
    $an_pai->nome = 'NOME DO PAI';
    $an_pai->brinco1 = '1';
    $an_pai->brinco2 = '15';
    $an_pai->tatuagem = '1123';
    $an_pai->raca = 'MESTIÇO';
    $an_pai->sexo = 'MACHO';
    $an_pai->categoria = 'BEZERRO';
    $an_pai->peso = '1000';
    $an_pai->idade = '1 ANO';
    $an_pai->obs = 'QUALQUER OBSERVAÇÕES CRIADA NA FICHA DO ANIMAL PAI';

    $an_mae = new animal_mae;
    $an_mae->nome = 'NOME DA MÃE';
    $an_mae->brinco1 = '1';
    $an_mae->brinco2 = '15';
    $an_mae->tatuagem = '1123';
    $an_mae->raca = 'MESTIÇO';
    $an_mae->sexo = 'MACHO';
    $an_mae->categoria = 'BEZERRO';
    $an_mae->peso = '1000';
    $an_mae->idade = '1 ANO';
    $an_mae->obs = 'QUALQUER OBSERVAÇÕES CRIADA NA FICHA DO ANIMAL MÃE';

    $an = new animal;
    $an->item = 1;
    $an->id = 1;
    $an->fazenda = $dadosFazenda;
    $an->nome = 'BOI MANSO';
    $an->brinco1 = '1';
    $an->brinco2 = '15';
    $an->tatuagem = '1123';
    $an->raca = 'MESTIÇO';
    $an->sexo = 'MACHO';
    $an->categoria = 'BEZERRO';
    $an->peso = 1000;
    $an->pesovlr = 10;
    $an->vlracrescimos = 30;
    $an->vlrdescontos = 20;
    $an->vlrtotal = 10;
    $an->idade = '1 ANO';
    $an->pai = $an_pai;
    $an->mae = $an_mae;
    $an->obs = 'QUALQUER OBSERVAÇÕES CRIADA NA FICHA DO ANIMAL';
    $an->link = "https://carnauba.sabidos.com.br/fazenda/ficha/42340348708242837401298378";
    $an->situacao = $situacao;
    $an->raca = $raca;
    $an->categoria = $categoria;
    $an->nascimento_at = '2019-01-01';
    $an->compra_at = '2019-01-02';
    $an->venda_at = '2019-01-03';
    $an->baixa_at = '2019-01-04';



    class movimento
    {
        public $item;
        public $quantidade;
        public $codigo;
        public $descricao;
        public $localizacao;
        public $vlrunitario;
        public $vlracrescimos;
        public $vlrdescontos;
        public $vlrtotal;
        public $situacao;
    }

    class compraAnimal
    {
        public $compra;
        public $situacao;
        public $created_at;
        public $conclusao_at;
        public $cancelamento_at;
        public $updated_at;
        public $obs;
    }

    $obj = new compraAnimal;
    $obj->id = 1;
    $obj->situacao = 'ABERTA';
    $obj->created_at = '2019-01-01 20:20:20';
    $obj->conclusao_at = '2019-01-01 20:20:20';
    $obj->cancelamento_at = '2019-01-01 20:20:20';
    $obj->empresa = $emp;
    $obj->fornecedor = $forn;
    $obj->animais = [$an];
    $obj->obs = 'Esta impressão é um simples teste de pdf, tem que gerar alguma coisa aqui se não num da certo..!!!!';

    $debug = false;

    if($debug){
        echo '<pre>';
        var_dump($obj);
        echo '</pre>';
    }

    $danfe = new imprimirCompra( $obj,'P', 'A4', $logo, 'I', '');
    $id = $danfe->montaPDF();
    $pdf = $danfe->render();
    header('Content-Type: application/pdf');
    echo $pdf;
} catch (InvalidArgumentException $e) {
    echo "Ocorreu um erro durante o processamento :" . $e->getMessage();
}    

